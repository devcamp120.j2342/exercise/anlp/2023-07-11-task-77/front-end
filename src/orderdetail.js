"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gOrderDetailTable = $("#orderDetail-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'STT' },
        { data: 'quantityOrder' },
        { data: 'priceEach' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function() {
              return stt++;
            }
          },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#orderDetail-table").on("click", ".fa-edit", function () {
        onDeatilorderDetailClick(this); // this là button được ấn
    });
    $("#orderDetail-table").on("click", ".fa-trash", function () {
        onDeleteorderDetailByIdClick(this); // this là button được ấn
    });
    $("#create-orderDetail").on("click", function () {
        onCreateNewOrderDetailClick();
    });
    $("#update-orderDetail").on("click", function () {
        onSaveorderDetailClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-orderDetail').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-orderDetail').on("click", function () {
        $('#modal-delete-all-orderDetail').modal('show');
    });
    $("#confirm-delete-all-orderDetail").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "orderDetails",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilorderDetailClick(paramChiTietButton) {
    debugger
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gOrderDetailTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "orderDetails/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadorderDetailToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteorderDetailByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-orderDetail').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gOrderDetailTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "orderDetails/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-orderDetail').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "orderDetail with id: "+ gId +" was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "orderDetails",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSaveorderDetailClick() {
    var newOrderDetail = {
        quantityOrder: $('#input-quantityOrder').val(paramOrderDetail.quantityOrder),
        priceEach: $('#input-firstName').val(paramOrderDetail.priceEach),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "orderDetails/" + gId,
        method: 'PUT',
        data: JSON.stringify(newOrderDetail),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetorderDetailInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewOrderDetailClick() {
    var newOrderDetail = {
        quantityOrder: $('#input-quantityOrder').val(paramOrderDetail.quantityOrder),
        priceEach: $('#input-firstName').val(paramOrderDetail.priceEach),
    };
    if (validateorderDetail(newOrderDetail)) {
        $.ajax({
            url: gBASE_URL + 'orderDetails',
            method: 'POST',
            data: JSON.stringify(newOrderDetail),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "orderDetail created successfully ",
                })
                onPageLoading();
                resetorderDetailInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gOrderDetailTable.clear();
    //Cập nhật data cho bảng 
    gOrderDetailTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gOrderDetailTable.draw();
}
// load dữ liệu vào form
function loadorderDetailToInput(paramOrderDetail) {
    $('#input-quantityOrder').val(paramOrderDetail.quantityOrder);
    $('#input-priceEach').val(paramOrderDetail.priceEach);
}
function resetorderDetailInput() {
    $('#input-quantityOrder').val('');
    $('#input-priceEach').val('');
}

function validateorderDetail(paramObj) {
    if (paramObj.quantityOrder == "") {
        toastCreateorderDetail("quantityOrder")
        return false;
    } else if (paramObj.priceEach == "") {
        toastCreateorderDetail("priceEach")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateorderDetail(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}