"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gemployeeTable = $("#employee-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'STT' },
        { data: 'lastName' },
        { data: 'firstName' },
        { data: 'extension' },
        { data: 'email' },
        { data: 'officeCode' },
        { data: 'reportTo' },
        { data: 'jobTitle' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function() {
              return stt++;
            }
          },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#employee-table").on("click", ".fa-edit", function () {
        onDeatilEmployeeClick(this); // this là button được ấn
    });
    $("#employee-table").on("click", ".fa-trash", function () {
        onDeleteemployeeByIdClick(this); // this là button được ấn
    });
    $("#create-employee").on("click", function () {
        onCreateNewEmployeeClick();
    });
    $("#update-employee").on("click", function () {
        onSaveEmployeeClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-employee').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-employee').on("click", function () {
        $('#modal-delete-all-employee').modal('show');
    });
    $("#confirm-delete-all-employee").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "employees",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilEmployeeClick(paramChiTietButton) {
    debugger
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gemployeeTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "employees/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loademployeeToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteemployeeByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-employee').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gemployeeTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "employees/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-employee').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "employee with id: "+ gId +" was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "employees",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSaveEmployeeClick() {
    var newEmployee = {
        lastName: $('#input-lastName').val(paramEmployee.lastName),
        firstName: $('#input-firstName').val(paramEmployee.firstName),
        extension: $('#input-ammount').val(paramEmployee.extension),
        email: $('#input-email').val(paramEmployee.email),
        officeCode: $('#input-officeCode').val(paramEmployee.officeCode),
        reportTo: $('#input-ammount').val(paramEmployee.reportTo),
        jobTitle: $('#input-ammount').val(paramEmployee.jobTitle),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "employees/" + gId,
        method: 'PUT',
        data: JSON.stringify(newEmployee),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetemployeeInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewEmployeeClick() {
    var newEmployee = {
        lastName: $('#input-lastName').val(paramEmployee.lastName),
        firstName: $('#input-firstName').val(paramEmployee.firstName),
        extension: $('#input-ammount').val(paramEmployee.extension),
        email: $('#input-email').val(paramEmployee.email),
        officeCode: $('#input-officeCode').val(paramEmployee.officeCode),
        reportTo: $('#input-ammount').val(paramEmployee.reportTo),
        jobTitle: $('#input-ammount').val(paramEmployee.jobTitle),
    };
    if (validateemployee(newEmployee)) {
        $.ajax({
            url: gBASE_URL + 'employees',
            method: 'POST',
            data: JSON.stringify(newEmployee),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "employee created successfully ",
                })
                onPageLoading();
                resetemployeeInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gemployeeTable.clear();
    //Cập nhật data cho bảng 
    gemployeeTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gemployeeTable.draw();
}
// load dữ liệu vào form
function loademployeeToInput(paramEmployee) {
    $('#input-lastName').val(paramEmployee.lastName);
    $('#input-firstName').val(paramEmployee.firstName);
    $('#input-extension').val(paramEmployee.extension);
    $('#input-email').val(paramEmployee.email);
    $('#input-officeCode').val(paramEmployee.officeCode);
    $('#input-reportTo').val(paramEmployee.reportTo);
    $('#input-jobTitle').val(paramEmployee.jobTitle);

}
function resetemployeeInput() {
    $('#input-lastName').val('');
    $('#input-firstName').val('');
    $('#input-extension').val('');
    $('#input-email').val('');
    $('#input-officeCode').val('');
    $('#input-reportTo').val('');
    $('#input-jobTitle').val('');

}

function validateemployee(paramObj) {
    if (paramObj.lastName == "") {
        toastCreateOrder("lastName")
        return false;
    } else if (paramObj.firstName == "") {
        toastCreateOrder("firstName")
        return false;
    } else if (paramObj.extension == "") {
        toastCreateOrder("extension")
        return false;
    } else if (paramObj.email == "") {
        toastCreateOrder("email")
        return false;
    } else if (paramObj.officeCode == "") {
        toastCreateOrder("officeCode")
        return false;
    } else if (paramObj.reportTo == "") {
        toastCreateOrder("reportTo")
        return false;
    } else if (paramObj.jobTitle == "") {
        toastCreateOrder("jobTitle")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}