"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gCustomerTable = $("#customer-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'STT' },
        { data: 'fullName' },
        { data: 'phoneNumber' },
        { data: 'address' },
        { data: 'city' },
        { data: 'state' },
        { data: 'postalCode' },
        { data: 'country' },
        { data: 'salesRepEmployeeNumber' },
        { data: 'creditLimit' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function () {
                return stt++;
            }
        },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#customer-table").on("click", ".fa-edit", function () {
        onDeatilCustomerClick(this); // this là button được ấn
    });
    $("#customer-table").on("click", ".fa-trash", function () {
        onDeleteCustomerByIdClick(this); // this là button được ấn
    });
    $("#create-customer").on("click", function () {
        onCreateNewCustomerClick();
    });
    $("#update-customer").on("click", function () {
        onSaveCustomerClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-customer').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-customer').on("click", function () {
        $('#modal-delete-all-customer').modal('show');
    });
    $("#confirm-delete-all-customer").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "customers",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            // Dữ liệu đã được tải xuống thành công
            var formattedData = responseObject.map(function (customer) {
                return {
                    id: customer.id,
                    fullName: customer.firstName + ' ' + customer.lastName,
                    phoneNumber: customer.phoneNumber,
                    address: customer.address,
                    city: customer.city,
                    state: customer.state,
                    postalCode: customer.postalCode,
                    country: customer.country,
                    salesRepEmployeeNumber: customer.salesRepEmployeeNumber,
                    creditLimit: customer.creditLimit,
                    action: customer.action
                };
            });
            loadDataToTable(formattedData);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilCustomerClick(paramChiTietButton) {
    debugger
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gCustomerTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "customers/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadCustomerToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteCustomerByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-customer').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gCustomerTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "customers/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-customer').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "Customer with id: " + gId + " was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "customers",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSaveCustomerClick() {
    var newCustomer = {
        lastName: $('#input-lastName').val(paramCustomer.lastName),
        firstName: $('#input-firstName').val(paramCustomer.firstName),
        phoneNumber: $('#input-phoneNumber').val(paramCustomer.phoneNumber),
        address: $('#input-address').val(paramCustomer.address),
        city: $('#input-city').val(paramCustomer.city),
        state: $('#input-state').val(paramCustomer.state),
        postalCode: $('#input-postalCode').val(paramCustomer.postalCode),
        country: $('#input-country').val(paramCustomer.country),
        salesRepEmployeeNumber: $('#input-salesRepEmployeeNumber').val(paramCustomer.salesRepEmployeeNumber),
        creditLimit: $('#input-creditLimit').val(paramCustomer.creditLimit)
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "customers/" + gId,
        method: 'PUT',
        data: JSON.stringify(newCustomer),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetCustomerInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewCustomerClick() {
    var newCustomer = {
        lastName: $('#input-lastName').val(paramCustomer.lastName),
        firstName: $('#input-firstName').val(paramCustomer.firstName),
        phoneNumber: $('#input-phoneNumber').val(paramCustomer.phoneNumber),
        address: $('#input-address').val(paramCustomer.address),
        city: $('#input-city').val(paramCustomer.city),
        state: $('#input-state').val(paramCustomer.state),
        postalCode: $('#input-postalCode').val(paramCustomer.postalCode),
        country: $('#input-country').val(paramCustomer.country),
        salesRepEmployeeNumber: $('#input-salesRepEmployeeNumber').val(paramCustomer.salesRepEmployeeNumber),
        creditLimit: $('#input-creditLimit').val(paramCustomer.creditLimit)
    };
    if (validateCustomer(newCustomer)) {
        $.ajax({
            url: gBASE_URL + 'customers',
            method: 'POST',
            data: JSON.stringify(newCustomer),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "Customer created successfully ",
                })
                onPageLoading();
                resetCustomerInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gCustomerTable.clear();
    //Cập nhật data cho bảng 
    gCustomerTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gCustomerTable.draw();
}
// load dữ liệu vào form
function loadCustomerToInput(paramCustomer) {
    $('#input-lastName').val(paramCustomer.lastName);
    $('#input-firstName').val(paramCustomer.firstName);
    $('#input-phoneNumber').val(paramCustomer.phoneNumber);
    $('#input-address').val(paramCustomer.address);
    $('#input-city').val(paramCustomer.city);
    $('#input-state').val(paramCustomer.state);
    $('#input-postalCode').val(paramCustomer.postalCode);
    $('#input-country').val(paramCustomer.country);
    $('#input-salesRepEmployeeNumber').val(paramCustomer.salesRepEmployeeNumber);
    $('#input-creditLimit').val(paramCustomer.creditLimit);
}
function resetCustomerInput() {
    $('#input-lastName').val('');
    $('#input-firstName').val('');
    $('#input-phoneNumber').val('');
    $('#input-address').val('');
    $('#input-city').val('');
    $('#input-state').val('');
    $('#input-postalCode').val('');
    $('#input-country').val('');
    $('#input-salesRepEmployeeNumber').val('');
    $('#input-creditLimit').val('');
}

function validateCustomer(paramObj) {
    if (paramObj.lastName == "") {
        toastCreateOrder("lastName")
        return false;
    } else if (paramObj.firstName == "") {
        toastCreateOrder("firstName")
        return false;
    } else if (paramObj.phoneNumber == "") {
        toastCreateOrder("phoneNumber")
        return false;
    } else if (paramObj.address == "") {
        toastCreateOrder("address")
        return false;
    } else if (paramObj.city == "") {
        toastCreateOrder("city")
        return false;
    } else if (paramObj.state == "") {
        toastCreateOrder("state")
        return false;
    } else if (paramObj.postalCode == "") {
        toastCreateOrder("postalCode")
        return false;
    } else if (paramObj.country == "") {
        toastCreateOrder("country")
        return false;
    } else if (paramObj.salesRepEmployeeNumber == "") {
        toastCreateOrder("salesRepEmployeeNumber")
        return false;
    } else if (paramObj.creditLimit == "") {
        toastCreateOrder("creditLimit")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}