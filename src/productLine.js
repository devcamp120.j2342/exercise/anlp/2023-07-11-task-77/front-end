"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gProductLineTable = $("#productLine-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'STT' },
        { data: 'productLine' },
        { data: 'description' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function() {
              return stt++;
            }
          },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#productLine-table").on("click", ".fa-edit", function () {
        onDeatilProductLineClick(this); // this là button được ấn
    });
    $("#productLine-table").on("click", ".fa-trash", function () {
        onDeleteProductLineByIdClick(this); // this là button được ấn
    });
    $("#create-productLine").on("click", function () {
        onCreateNewProductLineClick();
    });
    $("#update-productLine").on("click", function () {
        onSaveProductLineClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-productLine').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-productLine').on("click", function () {
        $('#modal-delete-all-productLine').modal('show');
    });
    $("#confirm-delete-all-productLine").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "productLines",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilProductLineClick(paramChiTietButton) {
    debugger
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gProductLineTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "productLines/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadproductLineToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteProductLineByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-productLine').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gProductLineTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "productLines/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-productLine').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "productLine with id: "+ gId +" was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "productLines",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSaveProductLineClick() {
    var newProductLine = {
        productLine: $('#input-productLine').val(paramproductLine.productLine),
        description: $('#input-description').val(paramproductLine.description),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "productLines/" + gId,
        method: 'PUT',
        data: JSON.stringify(newProductLine),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetproductLineInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewProductLineClick() {
    var newProductLine = {
        productLine: $('#input-productLine').val(paramproductLine.productLine),
        description: $('#input-description').val(paramproductLine.description),
    };
    if (validateproductLine(newProductLine)) {
        $.ajax({
            url: gBASE_URL + 'productLines',
            method: 'POST',
            data: JSON.stringify(newProductLine),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "productLine created successfully ",
                })
                onPageLoading();
                resetproductLineInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gProductLineTable.clear();
    //Cập nhật data cho bảng 
    gProductLineTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gProductLineTable.draw();
}
// load dữ liệu vào form
function loadproductLineToInput(paramproductLine) {
    $('#input-productLine').val(paramproductLine.productLine);
    $('#input-description').val(paramproductLine.description);

}
function resetproductLineInput() {
    $('#input-productLine').val('');
    $('#input-description').val('');
}

function validateproductLine(paramObj) {
    if (paramObj.productLine == "") {
        toastCreateOrder("productLine")
        return false;
    } else if (paramObj.description == "") {
        toastCreateOrder("description")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}