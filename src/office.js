"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gOfficeTable = $("#office-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'STT' },
        { data: 'city' },
        { data: 'phone' },
        { data: 'addressLine' },
        { data: 'state' },
        { data: 'country' },
        { data: 'territory' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function() {
              return stt++;
            }
          },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#office-table").on("click", ".fa-edit", function () {
        onDeatilOfficeClick(this); // this là button được ấn
    });
    $("#office-table").on("click", ".fa-trash", function () {
        onDeleteOfficeByIdClick(this); // this là button được ấn
    });
    $("#create-office").on("click", function () {
        onCreateNewOfficeClick();
    });
    $("#update-office").on("click", function () {
        onSaveOfficeClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-office').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-office').on("click", function () {
        $('#modal-delete-all-office').modal('show');
    });
    $("#confirm-delete-all-office").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "offices",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilOfficeClick(paramChiTietButton) {
    debugger
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gOfficeTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "offices/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadofficeToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteOfficeByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-office').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gOfficeTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "offices/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-office').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "office with id: "+ gId +" was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "offices",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSaveOfficeClick() {
    var newOffice = {
        city: $('#input-city').val(paramoffice.city),
        phone: $('#input-phone').val(paramoffice.phone),
        addressLine: $('#input-addressLine').val(paramoffice.addressLine),
        state: $('#input-state').val(paramoffice.state),
        country: $('#input-country').val(paramoffice.country),
        territory: $('#input-territory').val(paramoffice.territory),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "offices/" + gId,
        method: 'PUT',
        data: JSON.stringify(newOffice),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetofficeInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewOfficeClick() {
    var newOffice = {
        city: $('#input-city').val(paramoffice.city),
        phone: $('#input-phone').val(paramoffice.phone),
        addressLine: $('#input-addressLine').val(paramoffice.addressLine),
        state: $('#input-state').val(paramoffice.state),
        country: $('#input-country').val(paramoffice.country),
        territory: $('#input-territory').val(paramoffice.territory),
    };
    if (validateoffice(newOffice)) {
        $.ajax({
            url: gBASE_URL + 'offices',
            method: 'POST',
            data: JSON.stringify(newOffice),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "office created successfully ",
                })
                onPageLoading();
                resetofficeInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gOfficeTable.clear();
    //Cập nhật data cho bảng 
    gOfficeTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gOfficeTable.draw();
}
// load dữ liệu vào form
function loadofficeToInput(paramoffice) {
    $('#input-city').val(paramoffice.city);
    $('#input-phone').val(paramoffice.phone);
    $('#input-addressLine').val(paramoffice.addressLine);
    $('#input-state').val(paramoffice.state);
    $('#input-country').val(paramoffice.country);
    $('#input-territory').val(paramoffice.territory);
}
function resetofficeInput() {
    $('#input-city').val('');
    $('#input-orderDate').val('');
    $('#input-addressLine').val('');
    $('#input-state').val('');
    $('#input-country').val('');
    $('#input-territory').val('');

}

function validateoffice(paramObj) {
    if (paramObj.city == "") {
        toastCreateOrder("city")
        return false;
    } else if (paramObj.phone == "") {
        toastCreateOrder("phone")
        return false;
    } else if (paramObj.addressLine == "") {
        toastCreateOrder("addressLine")
        return false;
    } else if (paramObj.state == "") {
        toastCreateOrder("state")
        return false;
    } else if (paramObj.country == "") {
        toastCreateOrder("country")
        return false;
    } else if (paramObj.territory == "") {
        toastCreateOrder("territory")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}