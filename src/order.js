"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gOrderTable = $("#order-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'STT' },
        { data: 'comments' },
        { data: 'orderDate' },
        { data: 'requiredDate' },
        { data: 'shippedDate' },
        { data: 'status' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function() {
              return stt++;
            }
          },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#order-table").on("click", ".fa-edit", function () {
        onDeatilorderClick(this); // this là button được ấn
    });
    $("#order-table").on("click", ".fa-trash", function () {
        onDeleteorderByIdClick(this); // this là button được ấn
    });
    $("#create-order").on("click", function () {
        onCreateNewOrderClick();
    });
    $("#update-order").on("click", function () {
        onSaveorderClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-order').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-order').on("click", function () {
        $('#modal-delete-all-order').modal('show');
    });
    $("#confirm-delete-all-order").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "orders",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilorderClick(paramChiTietButton) {
    debugger
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gOrderTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "orders/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadOrderToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteorderByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-order').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gOrderTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "orders/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-order').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "order with id: "+ gId +" was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "orders",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSaveorderClick() {
    var newOrder = {
        comments: $('#input-comments').val(paramorder.comments),
        orderDate: $('#input-firstName').val(paramorder.orderDate),
        requiredDate: $('#input-ammount').val(paramorder.requiredDate),
        shippedDate: $('#input-ammount').val(paramorder.requiredDate),
        status: $('#input-ammount').val(paramorder.requiredDate),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "orders/" + gId,
        method: 'PUT',
        data: JSON.stringify(newOrder),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetOrderInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewOrderClick() {
    var newOrder = {
        comments: $('#input-comments').val(paramorder.comments),
        orderDate: $('#input-firstName').val(paramorder.orderDate),
        requiredDate: $('#input-ammount').val(paramorder.requiredDate),
        shippedDate: $('#input-ammount').val(paramorder.requiredDate),
        status: $('#input-ammount').val(paramorder.requiredDate),
    };
    if (validateOrder(newOrder)) {
        $.ajax({
            url: gBASE_URL + 'orders',
            method: 'POST',
            data: JSON.stringify(newOrder),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "order created successfully ",
                })
                onPageLoading();
                resetOrderInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gOrderTable.clear();
    //Cập nhật data cho bảng 
    gOrderTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gOrderTable.draw();
}
// load dữ liệu vào form
function loadOrderToInput(paramorder) {
    $('#input-comments').val(paramorder.comments);
    $('#input-orderDate').val(paramorder.orderDate);
    $('#input-requiredDate').val(paramorder.requiredDate);
    $('#input-shippedDate').val(paramorder.shippedDate);
    $('#input-status').val(paramorder.status);

}
function resetOrderInput() {
    $('#input-comments').val('');
    $('#input-orderDate').val('');
    $('#input-requiredDate').val('');
    $('#input-shippedDate').val('');
    $('#input-status').val('');
}

function validateOrder(paramObj) {
    if (paramObj.comments == "") {
        toastCreateOrder("comments")
        return false;
    } else if (paramObj.orderDate == "") {
        toastCreateOrder("orderDate")
        return false;
    } else if (paramObj.requiredDate == "") {
        toastCreateOrder("requiredDate")
        return false;
    } else if (paramObj.shippedDate == "") {
        toastCreateOrder("shippedDate")
        return false;
    } else if (paramObj.status == "") {
        toastCreateOrder("status")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}