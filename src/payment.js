"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gPaymentTable = $("#payment-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'STT' },
        { data: 'checkNumber' },
        { data: 'paymentDate' },
        { data: 'ammount' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function() {
              return stt++;
            }
          },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#payment-table").on("click", ".fa-edit", function () {
        onDeatilPaymentClick(this); // this là button được ấn
    });
    $("#payment-table").on("click", ".fa-trash", function () {
        onDeletePaymentByIdClick(this); // this là button được ấn
    });
    $("#create-payment").on("click", function () {
        onCreateNewPaymentClick();
    });
    $("#update-payment").on("click", function () {
        onSavePaymentClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-payment').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-payment').on("click", function () {
        $('#modal-delete-all-payment').modal('show');
    });
    $("#confirm-delete-all-payment").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "payments",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilPaymentClick(paramChiTietButton) {
    debugger
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gPaymentTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "payments/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadpaymentToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeletePaymentByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-payment').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gPaymentTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "payments/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-payment').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "payment with id: "+ gId +" was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "payments",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSavePaymentClick() {
    var newPayment = {
        checkNumber: $('#input-checkNumber').val(paramPayment.checkNumber),
        paymentDate: $('#input-firstName').val(paramPayment.paymentDate),
        ammount: $('#input-ammount').val(paramPayment.ammount),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "payments/" + gId,
        method: 'PUT',
        data: JSON.stringify(newPayment),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetpaymentInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewPaymentClick() {
    var newPayment = {
        checkNumber: $('#input-checkNumber').val(paramPayment.checkNumber),
        paymentDate: $('#input-paymentDate').val(paramPayment.paymentDate),
        ammount: $('#input-ammount').val(paramPayment.ammount),
    };
    if (validatepayment(newPayment)) {
        $.ajax({
            url: gBASE_URL + 'payments',
            method: 'POST',
            data: JSON.stringify(newPayment),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "payment created successfully ",
                })
                onPageLoading();
                resetpaymentInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gPaymentTable.clear();
    //Cập nhật data cho bảng 
    gPaymentTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gPaymentTable.draw();
}
// load dữ liệu vào form
function loadpaymentToInput(paramPayment) {
    $('#input-checkNumber').val(paramPayment.checkNumber);
    $('#input-paymentDate').val(paramPayment.paymentDate);
    $('#input-ammount').val(paramPayment.ammount);
}
function resetpaymentInput() {
    $('#input-checkNumber').val('');
    $('#input-orderDate').val('');
    $('#input-ammount').val('');
}

function validatepayment(paramObj) {
    if (paramObj.checkNumber == "") {
        toastCreateOrder("checkNumber")
        return false;
    } else if (paramObj.paymentDate == "") {
        toastCreateOrder("paymentDate")
        return false;
    } else if (paramObj.ammount == "") {
        toastCreateOrder("ammount")
        return false;
    } else if (paramObj.address == "") {
        toastCreateOrder("address")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}