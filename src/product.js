"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gProductTable = $("#product-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'STT' },
        { data: 'productCode' },
        { data: 'productName' },
        { data: 'productDescripttion' },
        { data: 'idProductLine' },
        { data: 'productScale' },
        { data: 'productVendor' },
        { data: 'quantityInStock' },
        { data: 'buyPrice' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function() {
              return stt++;
            }
          },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#product-table").on("click", ".fa-edit", function () {
        onDeatilProductClick(this); // this là button được ấn
    });
    $("#product-table").on("click", ".fa-trash", function () {
        onDeleteProductByIdClick(this); // this là button được ấn
    });
    $("#create-product").on("click", function () {
        onCreateNewProductClick();
    });
    $("#update-product").on("click", function () {
        onSaveProductClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-product').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-product').on("click", function () {
        $('#modal-delete-all-product').modal('show');
    });
    $("#confirm-delete-all-product").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "products",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilProductClick(paramChiTietButton) {
    debugger
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gProductTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "products/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadproductToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteProductByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-product').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gProductTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "products/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-product').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "product with id: "+ gId +" was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "products",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSaveProductClick() {
    var newProduct = {
        productCode: $('#input-productCode').val(paramproduct.productCode),
        productName: $('#input-productName').val(paramproduct.productName),
        productDescripttion: $('#input-productDescripttion').val(paramproduct.productDescripttion),
        idProductLine: $('#input-idProductLine').val(paramproduct.idProductLine),
        productScale: $('#input-productScale').val(paramproduct.productScale),
        productVendor: $('#input-productName').val(paramproduct.productVendor),
        quantityInStock: $('#input-productName').val(paramproduct.quantityInStock),
        buyPrice: $('#input-productName').val(paramproduct.buyPrice),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "products/" + gId,
        method: 'PUT',
        data: JSON.stringify(newProduct),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetproductInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewProductClick() {
    var newProduct = {
        productCode: $('#input-productCode').val(paramproduct.productCode),
        productName: $('#input-productName').val(paramproduct.productName),
        productDescripttion: $('#input-productDescripttion').val(paramproduct.productDescripttion),
        idProductLine: $('#input-idProductLine').val(paramproduct.idProductLine),
        productScale: $('#input-productScale').val(paramproduct.productScale),
        productVendor: $('#input-productName').val(paramproduct.productVendor),
        quantityInStock: $('#input-productName').val(paramproduct.quantityInStock),
        buyPrice: $('#input-productName').val(paramproduct.buyPrice),
    };
    if (validateproduct(newProduct)) {
        $.ajax({
            url: gBASE_URL + 'products',
            method: 'POST',
            data: JSON.stringify(newProduct),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "product created successfully ",
                })
                onPageLoading();
                resetproductInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gProductTable.clear();
    //Cập nhật data cho bảng 
    gProductTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gProductTable.draw();
}
// load dữ liệu vào form
function loadproductToInput(paramproduct) {
    $('#input-productCode').val(paramproduct.productCode);
    $('#input-productName').val(paramproduct.productName);
    $('#input-productDescripttion').val(paramproduct.productDescripttion);
    $('#input-idProductLine').val(paramproduct.idProductLine);
    $('#input-productScale').val(paramproduct.productScale);
    $('#input-productVendor').val(paramproduct.productVendor);
    $('#input-quantityInStock').val(paramproduct.quantityInStock);
    $('#input-buyPrice').val(paramproduct.buyPrice);
}
function resetproductInput() {
    $('#input-productCode').val('');
    $('#input-productName').val('');
    $('#input-productDescripttion').val('');
    $('#input-idProductLine').val('');
    $('#input-productScale').val('');
    $('#input-productVendor').val('');
    $('#input-quantityInStock').val('');
    $('#input-buyPrice').val('');

}

function validateproduct(paramObj) {
    if (paramObj.productCode == "") {
        toastCreateOrder("productCode")
        return false;
    } else if (paramObj.productName == "") {
        toastCreateOrder("productName")
        return false;
    } else if (paramObj.productDescripttion == "") {
        toastCreateOrder("productDescripttion")
        return false;
    } else if (paramObj.idProductLine == "") {
        toastCreateOrder("idProductLine")
        return false;
    } else if (paramObj.productScale == "") {
        toastCreateOrder("productScale")
        return false;
    } else if (paramObj.productVendor == "") {
        toastCreateOrder("productVendor")
        return false;
    } else if (paramObj.quantityInStock == "") {
        toastCreateOrder("quantityInStock")
        return false;
    } else if (paramObj.buyPrice == "") {
        toastCreateOrder("buyPrice")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}